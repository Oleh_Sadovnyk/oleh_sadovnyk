package hw1Lesson13;

import java.util.Scanner;

public class PhoneValidator {

    public static void main(String[] args) {

        System.out.print("Input Phone number:");
        Scanner in = new Scanner(System.in);
        String number;
        do {
            number = in.nextLine();
            if (phoneValidation(number)) {
                System.out.println("Phone number is accepted");
            } else {
                System.out.println("Phone number is not valid");
                System.out.print("Input Phone number:");
            }
        } while (!phoneValidation(number));
    }

    public static boolean phoneValidation(String num) {
        boolean phoneValid;
        String pattern = "^(\\s*)?([- ()+]?\\d){9,13}";
        phoneValid = num.matches(pattern);
        return phoneValid;
    }
}

